<?php
/* 
vous ajouterez ici les fonctions qui vous sont utiles dans le site,
je vous ai créé la première qui est pour le moment incomplète et qui devra contenir
la logique pour choisir la page à charger
*/
//_________________________________REDIRECT PAGE
function getContent(){
	if(!isset($_GET['page'])){
		include __DIR__.'/../pages/home.php';
	} 
	else {
		$page=$_GET['page'];
	}
	switch($page){
		case 'bio':
		include __DIR__.'/../pages/bio.php';
		break;
		case 'contact': 
		include __DIR__.'/../pages/contact.php';
		break;
		case 'home': 
		include __DIR__.'/../pages/home.php';
		break;
		
	} 
}
//_________________________________INCLUDE PAGE
function getPart($name){
	include __DIR__ . '/../parts/'. $name . '.php';
}
//_________________________________IMPORT DATA
function getUserData(){
// Lit 14 caractères à partir du 21ème charactère
$json = file_get_contents('../data/user.json');
$json = json_decode($json, true);

// foreach($json as $user){
	foreach($json as $clef => $valeur){
		if(is_array($valeur)){
			echo "$clef : <br>";
			for($o= 0; $o < count($valeur); $o++){
				foreach($valeur[$o] as $key => $val){
					echo $key.' : '.$val .'<br>';
				}
			}  
		}
		else{
			echo $clef.' : '.$valeur.'<br>';
		}
   };

};
function show_Message(){

	$messageSend = file_get_contents('../data/last_Message.json');
	$messageSend = json_decode($messageSend);
	
	foreach($messageSend as $key => $valeur){
		echo $key.' : '.$valeur.'<br>';
	};
}